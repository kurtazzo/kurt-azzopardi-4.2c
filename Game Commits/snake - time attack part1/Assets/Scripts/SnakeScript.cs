﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeScript : MonoBehaviour {

    //refeencing another snake script in the same script. 
    private SnakeScript next;

    //Creating the boxes so that the snake can move forward.
    public void Setnext(SnakeScript IN)
    {
        next = IN;
    }

    public SnakeScript GetNext()
    {
        return next;
    }

    public void RemoveTail()
    {
        //Destroy the last square.
        Destroy(this.gameObject);
    }
}
