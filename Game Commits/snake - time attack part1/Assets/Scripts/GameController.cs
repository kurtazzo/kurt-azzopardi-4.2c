﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    public GameObject snakePrefab;

    //creating 2 snake variables for the first and last square of the snake.
    public SnakeScript Head;
    public SnakeScript Tail;

    //creating a public integer to specify in which direction the snake is traveling.
    public int Direction;

    //will handle the new first square of the snake.
    public Vector2 newPosition;

    //creating 2 variables to control the size of the snake. Values entered from unity.
    public int maxSnake;
    public int currentSnake;

    //varaibles that will hold the range of the playable area. values entered from unity.
    public int xArea;
    public int yArea;

    public GameObject foodPrefab;
    public GameObject currentFood;

	// Use this for initialization
	void Start () {
        //repaeat the SnakeMoveTime function every 0.3 seconds starting from the start of the program (0).
        InvokeRepeating("SnakeMoveTime", 0, 0.3f);
        Food();
	}
    	
	// Update is called once per frame
	void Update () {
        DirectionChange();
	}

    void SnakeMoveTime()
    {
        Move();
        //if the current snake is at its max length, the snake must not grow longer so the function to remove the tail is called.
        if(currentSnake == maxSnake)
        {
            SnakeTail();
        }
        //if the snake is not at max length, the current snake will increase its length.
        else
        {
            currentSnake++;
        }
    }

    void Move()
    {
        GameObject temp;
        //setting the new position of the head varaible.
        newPosition = Head.transform.position;

        //switch statment that changes the direction of the snake depending on the direction key pressed.
        switch (Direction)
        {
            case 0:
                //the snake goes up.
                newPosition = new Vector2(newPosition.x, newPosition.y + 1);
                break;
            case 1:
                //the snake goes right.
                newPosition = new Vector2(newPosition.x + 1, newPosition.y);
                break;
            case 2:
                //the snake goes down.
                newPosition = new Vector2(newPosition.x, newPosition.y - 1);
                break;
            case 3:
                //the snake goes left.
                newPosition = new Vector2(newPosition.x - 1, newPosition.y);
                break;
        }

        //creating a clone of the object (the snake prefab) at the chosen psoition depending on the arrow key pressed. The new cloned object isnt rotated so the rotation is kept the same.
        temp = (GameObject)Instantiate(snakePrefab, newPosition, transform.rotation);

        //the new created object is saved as the head of the snake.
        Head.Setnext(temp.GetComponent<SnakeScript>());
        Head = temp.GetComponent<SnakeScript>();

        return;
    }

    //the function that will register the keys pressed and assign them to the correct movement.
    void DirectionChange()
    {
        //making sure that the snake isnt going down since if it goes down it will collide into itself.
        if (Direction != 2 && (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)))
        {
            //value to go up.
            Direction = 0;
        }
        //making sure that the snake isnt going left since if it goes down it will collide into itself.
        if (Direction != 3 && (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow)))
        {
            //value to go right.
            Direction = 1;
        }
        //making sure that the snake isnt going up since if it goes down it will collide into itself.
        if (Direction != 0 && (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)))
        {
            //value to go down.
            Direction = 2;
        }
        //making sure that the snake isnt going right since if it goes down it will collide into itself.
        if (Direction != 1 && (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow)))
        {
            //value to go left.
            Direction = 3;
        }
    }

    void SnakeTail()
    {
        //creating a varaible of type snakescript and getting equal to the variable tail.
        SnakeScript tempSnake = Tail;
        //setting the tail to the next square along the snake so the last one can be deleted.
        Tail = Tail.GetNext();
        //destorying the last square of the snake. function RemoveTail from SnakeScript.
        tempSnake.RemoveTail();
    }

    void Food()
    {
        //generating a random number for the x axis that can be seen on the playable area.
        int xRange = Random.Range(-xArea, xArea);
        //generating a random number for the y axis that can be seen on the playable area.
        int yRange = Random.Range(-yArea, yArea);

        //creating a clone of the object (the food) at a random generated positon that can be visiable on the playable area. the new cloned object isnt rotated so the roatation is kept the same.
        currentFood = (GameObject)Instantiate(foodPrefab, new Vector2(xRange, yRange), transform.rotation);
        //starting the coroutine and passing the variable currentFood to the function.
        StartCoroutine(FoodVisiblity(currentFood));
    }

    // [Defenition of coroutine] - A coroutine allows you to delay a method for a single amount of time or frames without pausing or stopping the rest of the program.

    IEnumerator FoodVisiblity(GameObject thisFood)
    {
        //wait to play the coroutine at the end of the end of the frame.
        yield return new WaitForEndOfFrame();
        //check if the food is visable on the playable area.
        if (thisFood.GetComponent<Renderer>().isVisible == false)
        {
            //check the tag of the GameObject.
            if(thisFood.tag == "Food")
            {
                //Destroy the food which isnt visible and respawn it.
                Destroy(thisFood);
                Food();
            }
        }
    }
}
