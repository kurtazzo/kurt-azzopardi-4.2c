﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SnakeScript : MonoBehaviour {


    //refeencing another snake script in the same script. 
    private SnakeScript next;

    //Creating the boxes so that the snake can move forward.
    public void Setnext(SnakeScript IN)
    {
        next = IN;
    }

    public SnakeScript GetNext()
    {
        return next;
    }

    public void RemoveTail()
    {
        Destroy(this.gameObject);
    }

    //when the snake collides with an object, this function will start.
    public void OnTriggerEnter2D(Collider2D thing)
    {
        Debug.Log("Touch Snake");

        //if the collision is with a food object, a boolean is switched to true.
        if(thing.tag == "Food")
        {
            Debug.Log("Eat Food - snake increased by 1");
            GameController.col = true;
        }
        
    }
}
