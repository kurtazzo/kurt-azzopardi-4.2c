﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {

    //Exit the current running game.
	public void Quit()
    {
        UnityEditor.EditorApplication.isPlaying = false;
    }

    //loads the scene with build index 1.
    public void Instructions()
    {
        SceneManager.LoadScene(1);
    }

    //loads the scene with build index 0.
    public void Back()
    {
        SceneManager.LoadScene(0);
    }

    //loads the scene with build index 2.
    public void play()
    {
        SceneManager.LoadScene(2);
    }

    //loads the scene with build index 3.
    public void go()
    {
        SceneManager.LoadScene(3);
    }
}
