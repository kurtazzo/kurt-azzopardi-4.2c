﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerController : MonoBehaviour {

    //holds the amount of time for how long the game will run.
    public int Seconds = 120;

    public Text Timer;

	// Use this for initialization
	void Start () {
        //repeating the function timercontrol each second.
        InvokeRepeating("TimerControl", 0, 1f);
	}

    // Update is called once per frame
    void Update()
    {
        //updating the seconds displayed on at the top left while playing.
        Timer.text = Seconds.ToString();
        //once the timer reaches 0, the gameover scene is loaded
        if(Seconds == 0)
        {
            SceneController.gameover();
        }
    }
    void TimerControl()
    {
        //decrease scobds by 1
        Seconds--;
        //every 10 seconds the speed is of the snake is increased. this is done by reducing the speed number causing the invokerepating to repeat faster.
        if (Seconds % 10 == 0)
        {
            GameController.speed = GameController.speed - 0.01f;
            Debug.Log("Speed increased - " + GameController.speed);
        }
    }
}
