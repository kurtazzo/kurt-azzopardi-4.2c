﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SnakeScript : MonoBehaviour {


    //refeencing another snake script in the same script.
    private SnakeScript next;

    //Creating the boxes so that the snake can move forward.
    public void Setnext(SnakeScript IN)
    {
        next = IN;
    }

    public SnakeScript GetNext()
    {
        return next;
    }

    public void RemoveTail()
    {
        Destroy(this.gameObject);
    }

    //when the snake collides with an object, this function will start.
    public void OnTriggerEnter2D(Collider2D touch)
    {
        //if the collision is with a food object, this boolean is switched to true.
        if(touch.tag == "Food")
        {
            Debug.Log("Collision with food");
            GameController.foodcol = true;
        }

        //if the collision is with a part of the snake, this boolean is switched to true.
        if(touch.tag == "Snake")
        {
            Debug.Log("Collision with self");
            GameController.deathcol = true;
        }

        //if the collision is with a part of the outside wall, this boolean is switched to true.
        if (touch.tag == "Outside")
        {
            Debug.Log("Collision with outside walls");
            GameController.deathcol = true;
        }

    }
}
