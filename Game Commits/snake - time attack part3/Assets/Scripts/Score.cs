﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    //variable that will hold the score of the player.
    public static int score = 0;

    //variable that will hold the score.
    public int highscore;

    //linking the unity UI components with the code.
    public Text CurrentScore;
    public Text CurrentScoreName;
    public Text HighScoreName;

    public InputField UserName;
    static string usernameinput;

	// Use this for initialization
	void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
        ScoreManager();
        
	}

    //Display the scores on the gameover screen.
    void ScoreManager()
    {
        
        //display the score every time it is updated.
        CurrentScore.text = score.ToString();
        //display the final score of the current game with username.
        CurrentScoreName.text = usernameinput + " - " + score.ToString();
        //checks if a new highscore is achieved and then display the highscore with username.
        if (highscore < score)
        {
            //set the new best score as the new highscore.
            highscore = score;
            HighScoreName.text = usernameinput + " - " + highscore.ToString();
        }
        
    }

    public void getusername()
    {
        //gets the value from unity ui once the user is done editing in the input field.
        usernameinput = UserName.text;
        Debug.Log("Username Entered - " + usernameinput);
    }
}
