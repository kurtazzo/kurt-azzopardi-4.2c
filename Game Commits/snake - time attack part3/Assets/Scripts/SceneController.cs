﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneController : MonoBehaviour {

    //boolean that holds the value weather the game is paused or not.
    public static bool paused = false;

    //boolean that holds the value weather the game is mmuted or not.
    public static bool mute = false;

    //linking the unity UI components with the code.
    public Button PauseButton;
    public Button MuteButton;

    //Exit the current running game.
	public void Quit()
    {
        UnityEditor.EditorApplication.isPlaying = false;
    }

    //loads the scene with build index 1.
    public void Instructions()
    {
        SceneManager.LoadScene(1);
    }

    //loads the scene with build index 0.
    public void Back()
    {
        SceneManager.LoadScene(0);
    }

    //loads the scene with build index 2.
    public void play()
    {
        SceneManager.LoadScene(2);
    }

    //loads the scene with build index 3.
    public void go()
    {
        SceneManager.LoadScene(3);
    }

    //loads the scene with build index 4.
    public static void gameover()
    {
        SceneManager.LoadScene(4);
    }

    public void pausegame()
    {
        paused = !paused;
        //Control at which the time is passing.
        if (paused)
        {
            //when 0 time freezes.
            Time.timeScale = 0;
            Debug.Log("Paused");
            //accessing the test of the button and changing it to Unpause.
            PauseButton.GetComponentInChildren<Text>().text = "Resume";
        }
        else
        {
            //when 1 time flows at a normal rate.
            Time.timeScale = 1;
            Debug.Log("UnPaused");
            //accessing the test of the button and changing it to Pause.
            PauseButton.GetComponentInChildren<Text>().text = "Pause";
        }
    }

    public void mutegame()
    {
        mute = !mute;
        if (mute)
        {
            Debug.Log("Muted");
            //accessing the test of the button and changing it to Unpause.
            MuteButton.GetComponentInChildren<Text>().text = "Unmute";
        }
        else
        {
            Debug.Log("Unmute");
            //accessing the test of the button and changing it to Mute.
            MuteButton.GetComponentInChildren<Text>().text = "Mute";
        }
    }
}
