﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    //gameobject that will hold the snake prefab.
    public GameObject snakePrefab;

    //creating 2 snake variables for the first and last square of the snake.
    public SnakeScript Head;
    public SnakeScript Tail;

    //creating a public integer to specify in which direction the snake is traveling.
    public int Direction;

    //will handle the new first square of the snake.
    public Vector2 newPosition;

    //creating 2 variables to control the size of the snake. Values entered from unity.
    public int maxSnake;
    public int currentSnake;

    //varaibles that will hold the range of the playable area. values entered from unity.
    public int xArea;
    public int yArea;

    //gameobjects that will hold the food prefabs.
    public GameObject foodPrefab;
    public GameObject currentFood;

	//gameobjects that will hold the powerup prefabs.
	public GameObject PowerupPrefab;
	public GameObject currentPowerup;

    //booleans that will turn true depending on the collisions that occur.
    public static bool foodcol = false;
	public static bool powercol = false;
    public static bool deathcol = false;

    //a float that can be edited from all scripts. holds the speed of the snake.
    public static float speed = 0.3f;

	//linking the unity UI text component with the code.
	public Text PowerDesc;

	//two integers that will hold a random nuber to indicate when the power up will spawn.
	int randpwrtime1;
	int randpwrtime2;

	//two booleans that indicates weather the powerups have been displayed or not.
	public bool PowerUpDisp1;
	public bool PowerUpDisp2;

	// Use this for initialization
	void Start () {
		//setting the speed to 0.3f so that if the game is replayed, the speed is reset.
		speed = 0.3f;
        //repaeat the SnakeMoveTime function every 0.3 seconds starting from the start of the program (0).
        InvokeRepeating("SnakeMoveTime", 0, speed);
        //spawning food in game.
        Food();
        //setting the score back to 0 for new game.
        Score.score = 0;
		//generating the random times the power ups will spawn.
		randpwrtime1 = Random.Range (70, 110);
		randpwrtime2 = Random.Range (10, 60);
		Debug.Log ("Power ups spawning at- " + randpwrtime1 + ", " + randpwrtime2);
		//setting the powerups to false since it have not been displayed since the start of the game.
		PowerUpDisp1 = false;
		PowerUpDisp2 = false;
	}
    	
	// Update is called once per frame
	void Update () {
        DirectionChange();
        Collisions();
		if (TimerController.Seconds == randpwrtime1 && !PowerUpDisp1) {
			Powerup ();
			//powerup displayed so the boolean is turned true.
			PowerUpDisp1 = true;
		}
		if (TimerController.Seconds == randpwrtime2 && !PowerUpDisp2) {
			Powerup ();
			//powerup displayed so the boolean is turned true.
			PowerUpDisp2 = true;
		}
	}

    void SnakeMoveTime()
    {
        Move();
        //if the current snake is at its max length, the snake must not grow longer so the function to remove the tail is called.
        if(currentSnake == maxSnake)
        {
            SnakeTail();
        }
        //if the snake is not at max length, the current snake will increase its length.
        else
        {
            currentSnake++;
        }
    }

    void Move()
    {
        GameObject temp;
        //setting the new position of the head varaible.
        newPosition = Head.transform.position;

        //switch statment that changes the direction of the snake depending on the direction key pressed.
        switch (Direction)
        {
            case 0:
                //the snake goes up.
                newPosition = new Vector2(newPosition.x, newPosition.y + 1);
                break;
            case 1:
                //the snake goes right.
                newPosition = new Vector2(newPosition.x + 1, newPosition.y);
                break;
            case 2:
                //the snake goes down.
                newPosition = new Vector2(newPosition.x, newPosition.y - 1);
                break;
            case 3:
                //the snake goes left.
                newPosition = new Vector2(newPosition.x - 1, newPosition.y);
                break;
        }

        //creating a clone of the object (the snake prefab) at the chosen psoition depending on the arrow key pressed. The new cloned object isnt rotated so the rotation is kept the same.
        temp = (GameObject)Instantiate(snakePrefab, newPosition, transform.rotation);

        //the new created object is saved as the head of the snake.
        Head.Setnext(temp.GetComponent<SnakeScript>());
        Head = temp.GetComponent<SnakeScript>();

        return;
    }

    //the function that will register the keys pressed and assign them to the correct movement.
    void DirectionChange()
    {
        //making sure that the snake isnt going down since if it goes down it will collide into itself.
        if (Direction != 2 && (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)))
        {
            //value to go up.
            Direction = 0;
        }
        //making sure that the snake isnt going left since if it goes down it will collide into itself.
        if (Direction != 3 && (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow)))
        {
            //value to go right.
            Direction = 1;
        }
        //making sure that the snake isnt going up since if it goes down it will collide into itself.
        if (Direction != 0 && (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)))
        {
            //value to go down.
            Direction = 2;
        }
        //making sure that the snake isnt going right since if it goes down it will collide into itself.
        if (Direction != 1 && (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow)))
        {
            //value to go left.
            Direction = 3;
        }
    }

    void SnakeTail()
    {
        //creating a varaible of type snakescript and getting equal to the variable tail.
        SnakeScript tempSnake = Tail;
        //setting the tail to the next square along the snake so the last one can be deleted.
        Tail = Tail.GetNext();
        //destorying the last square of the snake. function RemoveTail from SnakeScript.
        tempSnake.RemoveTail();
    }

    void Food()
    {
        //generating a random number for the x axis that can be seen on the playable area.
        int xRange = Random.Range(-xArea, xArea);
        //generating a random number for the y axis that can be seen on the playable area.
        int yRange = Random.Range(-yArea, yArea);

        //creating a clone of the object (the food) at a random generated positon that can be visiable on the playable area. the new cloned object isnt rotated so the roatation is kept the same.
        currentFood = (GameObject)Instantiate(foodPrefab, new Vector2(xRange, yRange), transform.rotation);
        //starting the coroutine and passing the variable currentFood to the function.
        StartCoroutine(FoodVisiblity(currentFood));
    }

    // [Defenition of coroutine] - A coroutine allows you to delay a method for a single amount of time or frames without pausing or stopping the rest of the program.

    IEnumerator FoodVisiblity(GameObject thisFood)
    {
        //wait to play the coroutine at the end of the end of the frame.
        yield return new WaitForEndOfFrame();
        //check if the food is visable on the playable area.
        if (thisFood.GetComponent<Renderer>().isVisible == false)
        {
            //check the tag of the GameObject.
            if(thisFood.tag == "Food")
            {
                //Destroy the food which isnt visible and respawn it.
                Destroy(thisFood);
                Food();
            }
        }
    }

	void Powerup()
	{
		//generating a random number for the x axis that can be seen on the playable area.
		int xRange = Random.Range(-xArea, xArea);
		//generating a random number for the y axis that can be seen on the playable area.
		int yRange = Random.Range(-yArea, yArea);

		//creating a clone of the object (the powerup) at a random generated positon that can be visiable on the playable area. the new cloned object isnt rotated so the roatation is kept the same.
		currentPowerup = (GameObject)Instantiate(PowerupPrefab, new Vector2(xRange, yRange), transform.rotation);
		//starting the coroutine and passing the variable currentPowerup to the function.
		StartCoroutine(PowerupVisiblity(currentPowerup));
	}

	IEnumerator PowerupVisiblity(GameObject thisPowerup)
	{
		//wait to play the coroutine at the end of the end of the frame.
		yield return new WaitForEndOfFrame();
		//check if the powerup is visable on the playable area.
		if (thisPowerup.GetComponent<Renderer>().isVisible == false)
		{
			//check the tag of the GameObject.
			if(thisPowerup.tag == "Powerup")
			{
				//Destroy the powerup which isnt visible and respawn it.
				Destroy(thisPowerup);
				Powerup();
			}
		}
	}

    void Collisions()
    {
        //if the snake collides with the food the following if statment will run.
        if (foodcol)
        {
            //set the boolean back to false so that the following if statment stops executing unless the snake eats food again.
            foodcol = false;
            //destroys the eaten food.
            Destroy(currentFood);
            //spawns new food at a new random location.
            Food();
            //incriments the score by 10.
            Score.score += 10;
            //increases the length of the snake.
            maxSnake++;
            Debug.Log("Snake length - Head + " + maxSnake);
        }

        //if the snake collides with itself the following if statment will run.
        if (deathcol)
        {
            //set the boolean back to false so that if the game is run agian the snake isnt still dead.
            deathcol = false;
            //stop moving the snake (by stopping the Invoke repeating).
            CancelInvoke("SnakeMoveTime");
            //calling the gameover function from the SceneController script so a new scene is loaded.
            SceneController.gameover();
        }

		//if the snake collides with the powerup the following if statment will run.
		if (powercol) {
			//set the boolean back to false so that the following if statment stops executing unless the snake eats a powerup again.
			powercol = false;
			//destroys the eaten powerup.
			Destroy (currentPowerup);
			//generate a random number so that a random powerup is chosen.
			int randPower = Random.Range (1, 4);
			switch (randPower) 
			{
			//if the random number is 1, the speed is restored back to that of the start.
			case 1:
				speed = 0.3f;
				PowerDesc.text = "Speed Reduced";
				Debug.Log ("PowerUp - Reset speed to that of start");
				break;
			//if the random number is 2, 10 seconds are added to the timer.
			case 2:
				TimerController.Seconds += 10;
				PowerDesc.text = "10 Seconds Added";
				Debug.Log ("PowerUp - Added 10 seconds to timer");
				break;
			//if the random number is 3, 100 points are added to the score.
			case 3:
				Score.score += 100;
				PowerDesc.text = "100 Points Added";
				Debug.Log ("PowerUp - Added 100 points to score");
				break;
			}

		}

    }

}
