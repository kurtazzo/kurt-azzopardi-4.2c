﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour {

	//store the music clip.
	public AudioClip MusicClip;

	//storing the game object that holds the music clip.
	public AudioSource MusicSource;

	//boolean to check weather the music is playing or not.
	public static bool NotPlaying = true;

	// Use this for initialization
	void Start () {
		sound ();
    }
	
	// Update is called once per frame
	void Update () {
		//if either the mute or pause buttons are pressed, the sound is paused.
		if (SceneController.mute || SceneController.paused) {
			//pausing the background music.
			MusicSource.Pause ();
			//set the boolean to true so the sound can be turned back on.
			NotPlaying = true;
		} else {
			sound ();
		}
	}

	public void sound(){
		MusicSource.clip = MusicClip;
		if (NotPlaying) {
			Debug.Log ("Music Turned On");
			//Play the music.
			MusicSource.Play ();
			//Dont destroy the gameobject when changing from one scene to another.
			DontDestroyOnLoad (transform.gameObject);
			//set the boolean to false so that the sound plays only once and doesnt overlap on itself. 
			NotPlaying = false;
		}
	}
}
